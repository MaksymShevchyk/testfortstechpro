import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

let id = 0;

export default new Vuex.Store({
  state: {
    propertiesList: [
      {
        name: 'affiliate',
        title: 'Affiliate',
        orderTypeDefault: 'ASC',
        priority: 0
      },
      {
        name: 'balance',
        title: 'Balance',
        orderTypeDefault: 'DESC',
        priority: 0
      },
      {
        name: 'bonus_balance',
        title: 'Bonus balance',
        orderTypeDefault: 'DESC',
        priority: 0
      },
      {
        name: 'campaign',
        title: 'Campaign',
        orderTypeDefault: 'ASC',
        priority: 0
      },
      {
        name: 'cash_balance',
        title: 'Cash balance',
        orderTypeDefault: 'DESC',
        priority: 0
      },
      {
        name: 'country',
        title: 'Country',
        orderTypeDefault: 'ASC',
        priority: 0
      },
      {
        name: 'trader_points',
        title: 'Trader points',
        orderTypeDefault: 'DESC',
        priority: 0
      }
    ],
    addedProperties: []
  },
  mutations: {
    addItem(state, payload) {
      const newId = id++;
      state.addedProperties.push({ id: newId, ...payload });
    },
    changeItem(state, payload) {
      const result = state.addedProperties.find(i => i.id === payload.id);
      result.property = payload.name;
      result.order = payload.order;
    },
    deleteItem(state, id) {
      state.addedProperties = state.addedProperties.filter(i => i.id !== id);
    },
    changeTypeDefault(state, payload) {
      state.addedProperties.find(i => i.id === payload.id).order = payload.type;
    }
  },
  actions: {
    submitList({ state }) {
      let number = 0;
      const data = [];
      state.addedProperties.filter(i =>
        data.push({
          Property: i.property,
          Priority: number++,
          Order: i.order
        })
      );
      console.log(data);
      state.addedProperties = [];
    }
  },
  getters: {
    propertiesToShow(state) {
        return state.propertiesList.map(p => {
            const isPropertyHasChoosen = state.addedProperties.find(i => i.property === p.name);
            return {
                name: p.name,
                title: p.title,
                order: p.orderTypeDefault,
                disabled: !!isPropertyHasChoosen
            };
        });
    }
  }
});
